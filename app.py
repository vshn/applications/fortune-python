"""
Fortune Cookie Service
"""

import os
import subprocess
from flask import Flask, request, Response, jsonify
from flask.templating import render_template
from subprocess import run, PIPE
from random import randrange

app = Flask(__name__)  # Standard Flask app
version = '1.2-python'
port = 8080

# https://stackoverflow.com/a/62394945
hostname = subprocess.check_output('hostname').decode('utf8')

def get_fortune():
    number = randrange(1000)
    fortune = run('fortune', stdout=PIPE, text=True).stdout
    return number, fortune

# tag::router[]
@app.route("/")
def fortune():
    """
    Print a random, hopefully interesting, adage
    """
    number, fortune = get_fortune()
    accepts = request.headers['accept']
    if accepts == 'application/json':
        resp = jsonify({ 'number': number, 'message': fortune, 'version': version, 'hostname': hostname })
        return resp

    if accepts == 'text/plain':
        result = 'Fortune %s cookie of the day #%d:\n\n%s' % (version, number, fortune)
        resp = Response(result, mimetype='text/plain')
        return resp

    # In all other cases, respond with HTML
    html = render_template('fortune.html', number=number, message=fortune, version=version, hostname=hostname)
    resp = Response(html, mimetype='text/html')
    return resp
# end::router[]

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ.get('listenport', port))
